package com.hillel.ua.common;

import helper.PropertiesHelper;


import java.io.IOException;
import java.util.Optional;

public class UrlBuilder {


    public UrlBuilder() throws IOException {
        PropertiesHelper propertiesHelper = new PropertiesHelper();
    }

    public static <T> String buildFullUrl(final Class<T> type) {
        String commonUrl = PropertiesHelper.readProperty("sportchek.main.url");
        return Optional.ofNullable(type)
                .map(annotation -> type.getAnnotation(PartialUrl.class))
                .map(PartialUrl::value)
                .map(partialUrl -> String.format("%s/%s", commonUrl, partialUrl))
                .orElseThrow(() -> new IllegalStateException("Missing '@PartialUrl' in class!"));
    }
}
