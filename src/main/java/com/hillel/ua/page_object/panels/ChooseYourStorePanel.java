package com.hillel.ua.page_object.panels;

import com.hillel.ua.page_object.pages.AbstractPage;
import net.serenitybdd.core.pages.WebElementFacade;

public class ChooseYourStorePanel extends AbstractPanel {

    private static final String FIND_STORE_INPUT_LOCATOR = ".//input[@id='pickup-modal-choose__input']";
    private static final String SUBMIT_BUTTON = ".//input[@type='submit']";
    private static final String TEXT_MESSAGE = ".//p[@class='pickup-stores__empty']";

    public ChooseYourStorePanel(final WebElementFacade panelBaseLocation, final AbstractPage driverDelegate) {
        super(panelBaseLocation, driverDelegate);
    }

    public void typeStoreName (String name) {
        findBy(FIND_STORE_INPUT_LOCATOR).sendKeys(name);
    }

    public void clickSubmitButton () {
        findBy(SUBMIT_BUTTON).waitUntilVisible().click();
    }

    public String getTextMessage() {
       return findBy(TEXT_MESSAGE).waitUntilVisible().getText();
    }


}
