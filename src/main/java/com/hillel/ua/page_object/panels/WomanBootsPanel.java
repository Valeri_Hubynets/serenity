package com.hillel.ua.page_object.panels;

import com.hillel.ua.page_object.pages.AbstractPage;
import net.serenitybdd.core.pages.WebElementFacade;

public class WomanBootsPanel extends AbstractPanel{

    private static final String TITLE_LOCATOR = ".//h1[@class='global-page-header__title']";

    public WomanBootsPanel(WebElementFacade panelBaseLocation, AbstractPage driverDelegate) {
        super(panelBaseLocation, driverDelegate);
    }

    public String getTitleWomanBoots() {
        return findBy(TITLE_LOCATOR).getText();
    }
}
