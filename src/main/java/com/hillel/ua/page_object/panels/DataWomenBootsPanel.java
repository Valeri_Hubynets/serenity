package com.hillel.ua.page_object.panels;

import com.hillel.ua.page_object.pages.AbstractPage;
import net.serenitybdd.core.pages.WebElementFacade;

public class DataWomenBootsPanel extends AbstractPanel {

    private static final String SIZE_LOCATOR = ".//span[@class='option-tiles__item-title']";
    private static final String ADD_TO_CART_BUTTON = ".//div[@class='product-detail__add-cart']";

    public DataWomenBootsPanel(WebElementFacade panelBaseLocation, AbstractPage driverDelegate) {
        super(panelBaseLocation, driverDelegate);
    }

    public String getSize() {
        return findBy(SIZE_LOCATOR).getText();
    }

    public void clickSize() {
        findBy(SIZE_LOCATOR).waitUntilClickable().click();
    }

    public void clickAddToCartButton() {
        findBy(ADD_TO_CART_BUTTON).waitUntilClickable().click();
    }
}
