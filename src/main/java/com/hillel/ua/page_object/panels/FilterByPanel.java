package com.hillel.ua.page_object.panels;

import com.hillel.ua.page_object.pages.AbstractPage;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class FilterByPanel extends AbstractPanel {
    private static final String BRAND_LOCATOR = "//*[contains(text(),'ATOMIC')]";
    private static final String PRELOADER_LOCATOR = "//main[@class='page-content page-container_blurred']";
    private static final String NO_PRELOADER_LOCATOR = "//main[@class='page-content']";
    private static final String CLEAR_ALL_LOCATOR = "//a[@class='facets-side__footer-link facets-side__clear-link']";

    public FilterByPanel(WebElementFacade panelBaseLocation, AbstractPage driverDelegate) {
        super(panelBaseLocation, driverDelegate);
        getDriverDelegate().waitOnPage().until(ExpectedConditions.elementToBeClickable(By.xpath(BRAND_LOCATOR)));
    }

    public void clickBrandFilter() {
        findBy(BRAND_LOCATOR).waitUntilClickable().click();
        getDriverDelegate().waitOnPage().until(ExpectedConditions.presenceOfElementLocated(By.xpath(PRELOADER_LOCATOR)));
        getDriverDelegate().waitOnPage().until(ExpectedConditions.presenceOfElementLocated(By.xpath(NO_PRELOADER_LOCATOR)));
    }

    public void clickClearAll() {
        findBy(CLEAR_ALL_LOCATOR).waitUntilClickable().click();
        getDriverDelegate().waitOnPage().until(ExpectedConditions.presenceOfElementLocated(By.xpath(PRELOADER_LOCATOR)));
        getDriverDelegate().waitOnPage().until(ExpectedConditions.presenceOfElementLocated(By.xpath(NO_PRELOADER_LOCATOR)));
    }
}
