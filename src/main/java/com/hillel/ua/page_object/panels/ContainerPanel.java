package com.hillel.ua.page_object.panels;

import com.hillel.ua.page_object.pages.AbstractPage;
import javassist.runtime.Inner;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ContainerPanel extends AbstractPanel {
    private static final String PRODUCT_TITLE = "//span[@class='product-title-text']";
    private static final String FIRST_PRODUCT_TITLE = "//span[@class='product-title-text'][1]";
    private static final String PRODUCT_ITEMS_WITH_RATING = "//span[@class='rating__value']";


    public ContainerPanel(final WebElementFacade panelBaseLocation, final AbstractPage driverDelegate) {
        super(panelBaseLocation, driverDelegate);
    }

    public String getFirstTitle() {
        getDriverDelegate().waitOnPage().until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(FIRST_PRODUCT_TITLE)));
        getDriverDelegate().waitOnPage().until(ExpectedConditions.visibilityOfAllElements(findBy(FIRST_PRODUCT_TITLE)));
        return findBy(FIRST_PRODUCT_TITLE).getText();
    }

    public List<String> getProductItems() {
        getDriverDelegate().waitOnPage().until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(PRODUCT_TITLE)));
        getDriverDelegate().waitOnPage().until(ExpectedConditions.visibilityOfAllElements(findBy(PRODUCT_TITLE)));
        return findAll(PRODUCT_TITLE)
                .stream()
                .map(item -> item.getText())
                .collect(Collectors.toList());
    }

    public String getFirstTitleNow() {
        getDriverDelegate().waitOnPage().until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(FIRST_PRODUCT_TITLE)));
        getDriverDelegate().waitOnPage().until(ExpectedConditions.visibilityOfAllElements(findBy(FIRST_PRODUCT_TITLE)));
        return findBy(FIRST_PRODUCT_TITLE).getText();
    }

    public List<String> getRating() {
        findBy(PRODUCT_ITEMS_WITH_RATING).waitUntilPresent();
       return findAll(PRODUCT_ITEMS_WITH_RATING)
               .stream()
               .map(item -> item.getAttribute("style").replaceAll("\\D", ""))
               .collect(Collectors.toList());

    }



}
