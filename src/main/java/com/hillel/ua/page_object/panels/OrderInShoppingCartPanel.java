package com.hillel.ua.page_object.panels;

import com.hillel.ua.page_object.pages.AbstractPage;
import net.serenitybdd.core.pages.WebElementFacade;

public class OrderInShoppingCartPanel extends AbstractPanel {
    private static final String SIZE_LOCATOR = "//div[@class='media-box__content']/div[2]";
    private static final String TITLE_LOCATOR = ".//a[@class='sc-product__title-link']";

    public OrderInShoppingCartPanel(WebElementFacade panelBaseLocation, AbstractPage driverDelegate) {
        super(panelBaseLocation, driverDelegate);
    }

    public String getTitleOrder() {
        return findBy(TITLE_LOCATOR).getText();
    }
    public String getSizeOrder() {
        return findBy(SIZE_LOCATOR).getText().replaceAll("\\D", "");
    }
}
