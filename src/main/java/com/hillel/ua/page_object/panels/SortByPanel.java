package com.hillel.ua.page_object.panels;

import com.hillel.ua.page_object.pages.AbstractPage;
import net.serenitybdd.core.pages.WebElementFacade;

public class SortByPanel extends AbstractPanel {
    private static final String SORT_BY_BUTTON = ".//select[@data-module-type='FormDropdown']";
    private static final String RATING_HIGH_TO_LOW = "//select[@data-module-type='FormDropdown']/option[5]";

    public SortByPanel(WebElementFacade panelBaseLocation, AbstractPage driverDelegate) {
        super(panelBaseLocation, driverDelegate);
    }

    public void clickSortByButton() {
        findBy(SORT_BY_BUTTON).click();
    }

    public void selectRatingHighToLow() {
        findBy(RATING_HIGH_TO_LOW).click();
    }
}
