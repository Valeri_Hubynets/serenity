package com.hillel.ua.page_object.panels;

import com.hillel.ua.page_object.pages.AbstractPage;
import com.hillel.ua.page_object.pages.ShoppingCartPage;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class HeaderPanel extends AbstractPanel {

    private static final String YOUR_STORE_BUTTON = ".//a[@class='header-pickup-store__link-container']";
    private static final String CHOOSE_YOUR_STORE_LOCATOR = ".//form[@class='pickup-modal-choose']";
    private static final String SHOPPING_CART_BUTTON = ".//a[@class='header-cart__trigger drawer-ui__toggle']";
    private static final String SHOPPING_CART_MESSAGE = "//div[@class='header-cart__confirmation-message_text']";

    public HeaderPanel(final WebElementFacade panelBaseLocation, final AbstractPage driverDelegate) {
        super(panelBaseLocation, driverDelegate);
    }


    public void clickYourStoreButton() {
        findBy(YOUR_STORE_BUTTON).click();
    }
    public ChooseYourStorePanel getChooseYourStorePanel() {
        return new ChooseYourStorePanel(findBy(CHOOSE_YOUR_STORE_LOCATOR), getDriverDelegate());
    }
    public void clickCartButton() {
        getDriverDelegate().waitOnPage().until(ExpectedConditions.presenceOfElementLocated(By.xpath(SHOPPING_CART_MESSAGE)));
        findBy(SHOPPING_CART_BUTTON).waitUntilClickable().click();
    }
}
