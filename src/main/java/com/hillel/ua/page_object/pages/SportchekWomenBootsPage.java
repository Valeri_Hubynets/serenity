package com.hillel.ua.page_object.pages;

import com.hillel.ua.common.PartialUrl;
import com.hillel.ua.page_object.panels.DataWomenBootsPanel;
import com.hillel.ua.page_object.panels.WomanBootsPanel;
import org.openqa.selenium.WebDriver;

@PartialUrl(value = "categories/women/footwear/boots/rain-boots/product/hunter-womens-original-chelsea-rain-boots-333132095.html")
public class SportchekWomenBootsPage extends AbstractPage {

    private static final String DATA_LOCATOR = "//div[@class='product-detail__options']";
    private static final String WOMAN_BOOTS = "//div[@class='product-detail__preview-gallery']";

    public SportchekWomenBootsPage(WebDriver webDriver) {
        super(webDriver);
    }

    public WomanBootsPanel getWomanBootsPanel() {
        return new WomanBootsPanel(findBy(WOMAN_BOOTS), this);
    }

    public DataWomenBootsPanel getDataWomenBootsPanel() {
        return new DataWomenBootsPanel(find(DATA_LOCATOR), this);
    }
}
