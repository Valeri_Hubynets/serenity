package com.hillel.ua.page_object.pages;

import com.hillel.ua.common.PartialUrl;
import com.hillel.ua.page_object.panels.ContainerPanel;
import com.hillel.ua.page_object.panels.FilterByPanel;
import com.hillel.ua.page_object.panels.SortByPanel;
import org.openqa.selenium.WebDriver;

@PartialUrl(value = "categories/shop-by-sport/alpine-skiing/ski-packages.html")
public class SportchekSkiPage extends AbstractPage {

    private static final String CONTAINER_LOCATOR = "//div[@class='product-listing__container container']";
    private static final String SORT_BY_PANEL_LOCATOR = "//div[@class='product-listing__filter product-listing__filter_bottom-line container']";
    private static final String FILTER_BY_LOCATOR = "//div[@data-module-type='Facets']";

    public SportchekSkiPage(WebDriver webDriver) {
        super(webDriver);
    }

    public ContainerPanel getContainerPanel() {
        return new ContainerPanel(findBy(CONTAINER_LOCATOR), this);
    }

    public FilterByPanel getFilterByPanel() {
        return new FilterByPanel(findBy(FILTER_BY_LOCATOR), this);
    }

    public SortByPanel getSortByPanel() {
        return new SortByPanel(findBy(SORT_BY_PANEL_LOCATOR), this);
    }
}
