package com.hillel.ua.page_object.pages;

import com.hillel.ua.page_object.panels.OrderInShoppingCartPanel;
import org.openqa.selenium.WebDriver;

public class ShoppingCartPage extends AbstractPage {

    private static final String ORDER_IN_SHOPPING_CART_LOCATOR = "//div[@class='sc-product-list']/section";

    public ShoppingCartPage(WebDriver webDriver) {
        super(webDriver);
    }

    public OrderInShoppingCartPanel getOrderInShoppingCartPanel() {
        return new OrderInShoppingCartPanel(findBy(ORDER_IN_SHOPPING_CART_LOCATOR), this);
    }
}
