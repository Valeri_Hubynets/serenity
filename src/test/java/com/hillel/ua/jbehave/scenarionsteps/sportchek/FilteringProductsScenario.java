package com.hillel.ua.jbehave.scenarionsteps.sportchek;

import com.hillel.ua.serenity.steps.sportchek.ContainerSteps;
import com.hillel.ua.serenity.steps.sportchek.FilterByBrandSteps;
import com.hillel.ua.serenity.steps.sportchek.SportchekSkiPageSteps;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

import java.util.List;

public class FilteringProductsScenario {

    @Steps
    SportchekSkiPageSteps sportchekSkiPageSteps;

    @Steps
    FilterByBrandSteps filterByBrandSteps;

    @Steps
    ContainerSteps containerSteps;

    @Given("user opened site, next url: '$url'")
    public void openSkiSite(final String url) {
        System.out.println("URL: " + url);
        sportchekSkiPageSteps.openPage(url);
    }

    @When("user filters product by brand 'ATOMIC'")
    public void selectFilterAtomic() {
        filterByBrandSteps.selectFilterByBrand();
    }

    @Then("title products value should be contains '$expectedNameProduct'")
    public void verifyFilter(final String expectedNameProduct) {
        final List<String> actualNamesProduct = containerSteps.getProductItems();
        for (String item : actualNamesProduct)
            Assert.assertTrue("There are not filtered products!", item.contains(expectedNameProduct));
    }
}
