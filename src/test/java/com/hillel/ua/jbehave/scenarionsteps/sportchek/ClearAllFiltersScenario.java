package com.hillel.ua.jbehave.scenarionsteps.sportchek;

import com.hillel.ua.serenity.steps.sportchek.ContainerSteps;
import com.hillel.ua.serenity.steps.sportchek.FilterByBrandSteps;
import com.hillel.ua.serenity.steps.sportchek.SportchekSkiPageSteps;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

import java.util.List;

public class ClearAllFiltersScenario {

    @Steps
    SportchekSkiPageSteps sportchekSkiPageSteps;

    @Steps
    FilterByBrandSteps filterByBrandSteps;

    @Steps
    ContainerSteps containerSteps;

    @Given("user opened site: '$url'")
    public void openSkiSite(final String url) {
        System.out.println("URL: " + url);
        sportchekSkiPageSteps.openPage(url);
    }

    @When("user gets first product item without filters")
    public void getFirstProductTitleWithoutFilters() {
        containerSteps.getFirstTitle();
    }

    @When("user filters product by brand 'ATOMIC'")
    public void selectFilterAtomic() {
        filterByBrandSteps.selectFilterByBrand();
    }

    @When("title products value should be contains '$expectedNameProduct'")
    public void verifyFilterAtomic(final String expectedNameProduct) {
        final List<String> actualNamesProduct = containerSteps.getProductItems();
        for (String item : actualNamesProduct)
            Assert.assertTrue("There are not filtered products!", item.contains(expectedNameProduct));
    }

    @When("user resets all filters, click button 'clear all'")
    public void clickButtonClearAll() {
        filterByBrandSteps.clearAll();
    }


    @Then("product items shold be equals product items without filters")
    public void verifyFunctionClearAll() {
        final String actualTitleProduct = containerSteps.getFirstTitleNow();
        final String expectedTitleProduct = containerSteps.getFirstTitle();
        Assert.assertEquals(expectedTitleProduct, actualTitleProduct);
    }
}
