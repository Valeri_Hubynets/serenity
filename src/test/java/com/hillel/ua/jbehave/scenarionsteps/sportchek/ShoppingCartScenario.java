package com.hillel.ua.jbehave.scenarionsteps.sportchek;

import com.hillel.ua.page_object.pages.SportchekWomenBootsPage;
import com.hillel.ua.serenity.steps.sportchek.ShoppingCartSteps;
import com.hillel.ua.serenity.steps.sportchek.SportchekWomenBootsPageSteps;
import com.hillel.ua.serenity.steps.sportchek.WomanBootsProductSteps;
import helper.PropertiesHelper;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

import java.io.IOException;

public class ShoppingCartScenario {

    private static PropertiesHelper propertiesHelper;

    public ShoppingCartScenario() throws IOException {
        propertiesHelper = new PropertiesHelper();
    }

    @Steps
    SportchekWomenBootsPageSteps sportchekWomenBootsPageSteps;

    @Steps
    WomanBootsProductSteps womanBootsProductSteps;

    @Steps
    ShoppingCartSteps shoppingCartSteps;

    @Given("user opened product 'woman boots'")
    public void openSportchekWomanBootsPage() {
        sportchekWomenBootsPageSteps.openPageByPartialUrl();
    }
    @When("user adds product to cart")
    public void addToCart() {
        String expectedTitle = womanBootsProductSteps.getExpectedTitle();
        String expectedSize = womanBootsProductSteps.getExpectedSize();
        womanBootsProductSteps.clickSize();
        womanBootsProductSteps.clickAddToCart();
        Serenity.setSessionVariable("expectedTitle").to(expectedTitle);
        Serenity.setSessionVariable("expectedSize").to(expectedSize);
    }
    @Then("size and title product in shopping cart shold be equals product selected user")
    public void verifyProduct() {
        shoppingCartSteps.openShoppingCart();
        final String actualTitle = shoppingCartSteps.getActualTitle();
        final String actualSize = shoppingCartSteps.getActualSize();
        final String expectedTitle = Serenity.sessionVariableCalled("expectedTitle");
        final String expectedSize = Serenity.sessionVariableCalled("expectedSize");
        Assert.assertEquals("is not correct title", expectedTitle, actualTitle);
        Assert.assertEquals("is not correct size", expectedSize, actualSize);
    }

}
