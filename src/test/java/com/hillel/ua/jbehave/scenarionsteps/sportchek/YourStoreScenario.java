package com.hillel.ua.jbehave.scenarionsteps.sportchek;

import com.hillel.ua.serenity.steps.sportchek.HeaderPanelSteps;
import com.hillel.ua.serenity.steps.sportchek.SportCheckMainPageSteps;
import com.hillel.ua.serenity.steps.sportchek.YourStoreSteps;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.unitils.reflectionassert.ReflectionAssert;

public class YourStoreScenario {

    @Steps
    SportCheckMainPageSteps sportCheckMainPageSteps;

    @Steps
    HeaderPanelSteps headerPanelSteps;

    @Steps
    YourStoreSteps yourStoreSteps;

    @Given("user opened site url: '$url'")
    public void openSite(final String url) {
        System.out.println("URL: " + url);
        sportCheckMainPageSteps.openPage(url);
    }
    @When("user clicks 'Choose store now' button")
    public void clickStoreButton() {
        headerPanelSteps.openYourStore();

    }

    @When("user type store '$name'")
    public void typeStoreName (final String name) {
        yourStoreSteps.typeNameStore(name);
        yourStoreSteps.clickSubmitButton();
    }

    @Then("error message value should be: '$expectedTextMessage'")
    public void verifyErrorMessageValue (final String expectedTextMessage) {
        final String actualTextMessage = yourStoreSteps.getTextMessage();
        ReflectionAssert.assertReflectionEquals("There are incorrect error message displayed!", expectedTextMessage, actualTextMessage);
    }
}
