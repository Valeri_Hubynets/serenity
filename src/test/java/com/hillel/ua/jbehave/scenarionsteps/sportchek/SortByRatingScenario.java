package com.hillel.ua.jbehave.scenarionsteps.sportchek;

import com.hillel.ua.serenity.steps.sportchek.ContainerSteps;
import com.hillel.ua.serenity.steps.sportchek.SortBySteps;
import com.hillel.ua.serenity.steps.sportchek.SportchekSkiPageSteps;
import helper.PropertiesHelper;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

import java.io.IOException;
import java.util.List;

public class SortByRatingScenario {

    private static PropertiesHelper propertiesHelper;

    public SortByRatingScenario() throws IOException {
        propertiesHelper = new PropertiesHelper();
    }

    @Steps
    SportchekSkiPageSteps sportchekSkiPageSteps;

    @Steps
    SortBySteps sortBySteps;

    @Steps
    ContainerSteps containerSteps;

    @Given("user opened site 'Sportchek' ski page")
    public void openSportchekSkiPage() {
        sportchekSkiPageSteps.openPageByPartialUrl();
    }

    @When("user sort by 'Rating From High to Low'")
    public void RatingFromHighToLow() {
        sortBySteps.clickButtonSortBy();
        sortBySteps.selectSortByStars();
    }

    @Then("product items shold be sort by stars")
    public void VerifySorting() {
        final List<Integer> productItems = containerSteps.getRatingItems();

        int[] array = productItems.stream().mapToInt(i -> i).toArray();

        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                Assert.assertTrue("Sorting is broken", array[i] >= array[j]);
            }
            System.out.println(array[i] );
        }
        System.out.println("Products sort by Rating From High to Low is complete");
    }
}
