package com.hillel.ua.serenity.steps.sportchek;

import com.hillel.ua.common.UrlBuilder;
import com.hillel.ua.page_object.pages.SportchekSkiPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.Pages;
import net.thucydides.core.steps.ScenarioSteps;

public class SportchekSkiPageSteps extends ScenarioSteps {

    private SportchekSkiPage sportchekSkiPage;

    public SportchekSkiPageSteps(final Pages pages) {
        this.sportchekSkiPage = pages.getPage(SportchekSkiPage.class);
    }

    @Step
    public void openPage(final String url) {
        System.out.println("Opening url: " + url);
        sportchekSkiPage.openUrl(url);
    }

    @Step
    public void openPageByPartialUrl() {
        final String fullNavUrl = UrlBuilder.buildFullUrl(SportchekSkiPage.class);
        sportchekSkiPage.openUrl(fullNavUrl);
    }
}
