package com.hillel.ua.serenity.steps.sportchek;

import com.hillel.ua.page_object.pages.ShoppingCartPage;
import com.hillel.ua.page_object.pages.SportchekMainPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.Pages;
import net.thucydides.core.steps.ScenarioSteps;

public class ShoppingCartSteps extends ScenarioSteps {
    private ShoppingCartPage shoppingCartPage;
    private SportchekMainPage sportchekMainPage;

    public ShoppingCartSteps(final Pages pages) {
        this.shoppingCartPage = pages.getPage(ShoppingCartPage.class);
    }

    @Step
    public void openShoppingCart() {
        sportchekMainPage.getHeaderPanel().clickCartButton();
    }
    @Step
    public String getActualTitle() {
       return shoppingCartPage.getOrderInShoppingCartPanel().getTitleOrder();
    }
    @Step
    public String getActualSize() {
        return shoppingCartPage.getOrderInShoppingCartPanel().getSizeOrder();
    }
}
