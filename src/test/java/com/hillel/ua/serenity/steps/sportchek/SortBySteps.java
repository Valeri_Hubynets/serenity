package com.hillel.ua.serenity.steps.sportchek;

import com.hillel.ua.page_object.pages.SportchekSkiPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.Pages;
import net.thucydides.core.steps.ScenarioSteps;

public class SortBySteps extends ScenarioSteps {

    private SportchekSkiPage sportchekSkiPage;

    public SortBySteps(final Pages pages) {
        super(pages);
        this.sportchekSkiPage = pages.getPage(SportchekSkiPage.class);
    }

    @Step
    public void clickButtonSortBy() {
        sportchekSkiPage.getSortByPanel().clickSortByButton();
    }

    @Step
    public void selectSortByStars() {
        sportchekSkiPage.getSortByPanel().selectRatingHighToLow();

    }
}
