package com.hillel.ua.serenity.steps.sportchek;

import com.hillel.ua.page_object.pages.SportchekSkiPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.Pages;
import net.thucydides.core.steps.ScenarioSteps;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ContainerSteps extends ScenarioSteps {

    private SportchekSkiPage sportchekSkiPage;


    public ContainerSteps(final Pages pages) {
        super(pages);
        this.sportchekSkiPage = pages.getPage (SportchekSkiPage.class);
    }

    @Step
    public List<String> getProductItems() {
        return sportchekSkiPage.getContainerPanel().getProductItems();
    }

    @Step
    public String getFirstTitle() {
        return sportchekSkiPage.getContainerPanel().getFirstTitle();
    }

    @Step
    public String getFirstTitleNow() {
        return sportchekSkiPage.getContainerPanel().getFirstTitleNow();
    }

    @Step
    public List<Integer> getRatingItems() {


        ArrayList<String> listOfStrings = new ArrayList<>(sportchekSkiPage.getContainerPanel().getRating());
        return listOfStrings.stream()
                .map(Integer::parseInt)
                .collect(Collectors.toList());
    }


}
