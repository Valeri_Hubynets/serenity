package com.hillel.ua.serenity.steps.sportchek;

import com.hillel.ua.page_object.pages.SportchekSkiPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.Pages;
import net.thucydides.core.steps.ScenarioSteps;

import java.util.List;

public class FilterByBrandSteps extends ScenarioSteps {

    private SportchekSkiPage sportchekSkiPage;

    public FilterByBrandSteps(final Pages pages) {
        super(pages);
        this.sportchekSkiPage = pages.getPage (SportchekSkiPage.class);
    }

    @Step
    public void selectFilterByBrand() {
        sportchekSkiPage.getFilterByPanel().clickBrandFilter();
    }

    @Step
    public void clearAll() {
        sportchekSkiPage.getFilterByPanel().clickClearAll();
    }


}
