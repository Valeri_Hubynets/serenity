package com.hillel.ua.serenity.steps.sportchek;

import com.hillel.ua.common.UrlBuilder;
import com.hillel.ua.page_object.pages.SportchekWomenBootsPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.Pages;
import net.thucydides.core.steps.ScenarioSteps;

public class SportchekWomenBootsPageSteps extends ScenarioSteps {

    private SportchekWomenBootsPage sportchekWomenBootsPage;

    public SportchekWomenBootsPageSteps(final Pages pages) {
        this.sportchekWomenBootsPage = pages.getPage(SportchekWomenBootsPage.class);
    }

    @Step
    public void openPageByPartialUrl() {
        final String fullNavUrl = UrlBuilder.buildFullUrl(SportchekWomenBootsPage.class);
        sportchekWomenBootsPage.openUrl(fullNavUrl);
    }
}


