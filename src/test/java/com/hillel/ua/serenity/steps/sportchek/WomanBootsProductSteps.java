package com.hillel.ua.serenity.steps.sportchek;

import com.hillel.ua.page_object.pages.SportchekWomenBootsPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.Pages;
import net.thucydides.core.steps.ScenarioSteps;
import org.yecht.Data;

public class WomanBootsProductSteps extends ScenarioSteps {

    private SportchekWomenBootsPage sportchekWomenBootsPage;

    public WomanBootsProductSteps(final Pages pages) {
        this.sportchekWomenBootsPage = pages.getPage(SportchekWomenBootsPage.class);
    }

    @Step
    public String getExpectedTitle() {
       return sportchekWomenBootsPage.getWomanBootsPanel().getTitleWomanBoots();
    }
    @Step
    public String getExpectedSize() {
        return sportchekWomenBootsPage.getDataWomenBootsPanel().getSize();
    }
    @Step
    public void clickSize() {
        sportchekWomenBootsPage.getDataWomenBootsPanel().clickSize();
    }
    @Step
    public void clickAddToCart() {
        sportchekWomenBootsPage.getDataWomenBootsPanel().clickAddToCartButton();
    }

}
