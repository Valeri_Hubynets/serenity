package com.hillel.ua.serenity.steps.sportchek;

import com.hillel.ua.page_object.pages.SportchekMainPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.Pages;
import net.thucydides.core.steps.ScenarioSteps;

public class YourStoreSteps extends ScenarioSteps {

    private SportchekMainPage sportchekMainPage;

    public YourStoreSteps(final Pages pages) {
        super(pages);
        this.sportchekMainPage = pages.getPage(SportchekMainPage.class);
    }

    @Step
    public void typeNameStore (String name) {
      sportchekMainPage.getHeaderPanel().getChooseYourStorePanel().typeStoreName(name);
    }

    @Step
    public void clickSubmitButton() {
        sportchekMainPage.getHeaderPanel().getChooseYourStorePanel().clickSubmitButton();
    }

    @Step
    public String getTextMessage() {
        return sportchekMainPage.getHeaderPanel().getChooseYourStorePanel().getTextMessage();
    }
}

