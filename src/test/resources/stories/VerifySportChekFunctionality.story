Meta:

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal

Scenario: Verify choose store near our location (Homework Ex18)

Given user opened site url: 'https://www.sportchek.ca/'
When user clicks 'Choose store now' button
And user type store 'Valeri'
Then error message value should be: 'We're sorry, there are no stores near your location. Please try reentering location'

Scenario: Verify filtering by brand (Homework Ex20)

Given user opened site, next url: 'https://www.sportchek.ca/categories/shop-by-sport/alpine-skiing/ski-packages.html'
When user filters product by brand 'ATOMIC'
Then title products value should be contains 'Atomic'

Scenario: Verify reset all filters (Homework Ex21)

Given user opened site: 'https://www.sportchek.ca/categories/shop-by-sport/alpine-skiing/ski-packages.html'
When user gets first product item without filters
And user filters product by brand 'ATOMIC'
And title products value should be contains 'Atomic'
And user resets all filters, click button 'clear all'
Then product items shold be equals product items without filters

Scenario: Verify sort by Rating From High to Low (Homework Ex19)
Given user opened site 'Sportchek' ski page
When user sort by 'Rating From High to Low'
Then product items shold be sort by stars

Scenario: Verify add to shopping cart (Homework Ex22)

Given user opened product 'woman boots'
When user adds product to cart
Then size and title product in shopping cart shold be equals product selected user




